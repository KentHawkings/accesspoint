.PHONY: test

run:
	flask run

test:
	PYTHONPATH=. pytest
