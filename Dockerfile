FROM python:3.9-slim-buster

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV C_FORCE_ROOT=1

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

EXPOSE 5000

RUN pip3 install --upgrade pip && pip3 install poetry

COPY poetry.lock pyproject.toml /usr/src/app/
RUN poetry config virtualenvs.create false && poetry install -n
COPY . /usr/src/app

CMD ["flask", "run", "-h", "0.0.0.0"]

