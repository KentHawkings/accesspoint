import os

from flask import Flask
from flask_caching import Cache
from flask_cors import CORS
from flask_marshmallow import Marshmallow

ma = Marshmallow()
cache = Cache()


def create_app(test_config: dict = None) -> Flask:
    # create and configure the app
    app = Flask(__name__)

    if test_config:
        app.config.from_mapping(test_config)
    else:
        app.config.from_pyfile("config.py")

    initialize_extensions(app)
    register_blueprints(app)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    return app


def initialize_extensions(app: Flask):
    CORS(app)
    ma.init_app(app)
    cache.init_app(app)


def register_blueprints(app: Flask):
    from api import geolocation
    app.register_blueprint(geolocation.bp)
