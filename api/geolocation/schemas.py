from marshmallow import EXCLUDE

from api import ma


class AccessPointSchema(ma.Schema):
    bssid = ma.String(required=True)
    rssi = ma.Integer(required=True)
    timestamp = ma.Integer(required=True)
    channel = ma.String(required=True)

    class Meta:
        unknown = EXCLUDE
