import json

from flask import Blueprint
from flask import current_app as app
from flask import request
from marshmallow import ValidationError
from requests import HTTPError

from api import cache
from api.geolocation.schemas import AccessPointSchema
from api.geolocation.services import GeolocationService

bp = Blueprint("geolocate_bp", __name__, url_prefix="/geolocate")


def _make_cache_key():
    return hash(json.dumps(request.json.get('apscan_data')))


def _response_filter(response):
    try:
        return 'accuracy' in response and 'location' in response
    except KeyError:
        return False


@bp.route("/", methods=("POST",))
@cache.cached(
    response_filter=_response_filter,
    make_cache_key=_make_cache_key
)
def geolocate_view():
    service = GeolocationService(app.config["GOOGLE_API_KEY"])

    if request_json := request.json:
        data = request_json.get("apscan_data", [])
    else:
        data = []

    try:
        ap_data = AccessPointSchema(many=True).load(data)
        return service.geolocate_for_access_point_data(ap_data)
    except ValidationError as error:
        return error.messages, 400
    except HTTPError as error:
        return str(error), error.response.status_code
