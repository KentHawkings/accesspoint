import pytest
from flask import Flask
from flask.testing import FlaskClient

from api import create_app


@pytest.fixture(scope='module')
def flask_app() -> Flask:
    return create_app({
        'FLASK_ENV': 'development',
        'CACHE_TYPE': 'SimpleCache',
        'SECRET_KEY': 'test-secret',
        'TESTING': True,
        'GOOGLE_API_KEY': 'AIzaSyCCK6hPzvUI1_XbDCV4pC1HN_6bneUejYc'
    })


@pytest.fixture(scope='module')
def client(flask_app: Flask) -> FlaskClient:
    with flask_app.test_client() as test_client:
        with flask_app.app_context():
            yield test_client
