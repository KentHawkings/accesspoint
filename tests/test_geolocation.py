import json
import os.path
from pathlib import Path

from flask.testing import FlaskClient


def test_client(client: FlaskClient):
    with open(os.path.join(Path(__file__).resolve().parent, 'request_payload.json')) as payload:
        data = json.load(payload)

    response = client.post('/geolocate/', json=data)

    assert response.status_code == 200
    assert 'accuracy' in response.json
    assert 'location' in response.json
    location = response.json['location']
    assert -34 <= location['lat'] <= -33
    assert 25 <= location['lng'] <= 26
